package com.cjuca.thecat.base

data class ScreenUiData<T>(val state: State = State.IDLE, val data: T, val error: String? = null)

enum class State {
    LOADING,
    REFRESHING,
    IDLE,
    ERROR,
    EMPTY,
    SUCCESS
}