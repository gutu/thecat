package com.cjuca.thecat.detail.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cjuca.thecat.core.manager.CatsManager

class CatViewModelFactory(
    private val application: Application,
    private val manager: CatsManager
) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CatDetailViewModel::class.java)) {
            return CatDetailViewModel(application, manager) as T
        }
        throw IllegalArgumentException("This factory handle only CatDetailViewModel classes")
    }
}