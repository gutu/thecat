package com.cjuca.thecat.detail.ui.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.cjuca.thecat.base.ScreenUiData
import com.cjuca.thecat.base.State
import com.cjuca.thecat.core.manager.CatsManager
import com.cjuca.thecat.core.model.Sort
import com.cjuca.thecat.core.model.Type
import com.cjuca.thecat.detail.ui.data.CatDetailUiData
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers

class CatDetailViewModel(
    hostApplication: Application,
    private val manager: CatsManager
) : AndroidViewModel(hostApplication) {

    val dataSource: BehaviorProcessor<ScreenUiData<CatDetailUiData>> =
        BehaviorProcessor.createDefault(
            ScreenUiData(state = State.LOADING, data = CatDetailUiData())
        )
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }


    init {
        readCatDetail()
    }

    fun readCatDetail() {
        dataSource.value?.let { uiData ->
            compositeDisposable.add(
                manager.getAllImages(1, 1, Sort.NO_SORT, Type.STATIC.typeValue)
                    .subscribeOn(Schedulers.io()).subscribe({ catImages ->
                        val catImage = catImages.first()
                        val breeds = catImage.breeds.takeIf { it.isNotEmpty() }?.first()
                        dataSource.onNext(
                            uiData.copy(
                                state = State.IDLE, data = uiData.data.copy(
                                    id = catImage.id,
                                    image = catImage.url,
                                    name = breeds?.name ?: "",
                                    description = breeds?.description ?: "",
                                    origin = breeds?.origin ?: "",
                                    moreDetails = breeds?.moreDetails ?: "",
                                    energyLevel = breeds?.energy ?: 0f,
                                    childFriendly = breeds?.childFriendly ?: 0f
                                )
                            )
                        )
                    }, {
                        Log.e("CatDetailViewModel", "Failed to load categories", it)
                        dataSource.onNext(
                            uiData.copy(
                                state = State.ERROR,
                                error = "Loading cat error : $it"
                            )
                        )
                    })
            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}