package com.cjuca.thecat.detail.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.cjuca.thecat.R
import com.cjuca.thecat.base.State
import com.cjuca.thecat.base.loadImage
import com.cjuca.thecat.core.manager.CatsManager
import com.cjuca.thecat.detail.ui.viewmodel.CatDetailViewModel
import com.cjuca.thecat.detail.ui.viewmodel.CatViewModelFactory
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_detail.view.*
import kotlinx.android.synthetic.main.fragment_home.*

class CatDetailFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private lateinit var viewModel: CatDetailViewModel
    private val viewModelFactory by lazy {
        val context = this.requireActivity().application
        CatViewModelFactory(context, CatsManager(context))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(CatDetailViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_detail, container, false)

        root.otherButton.setOnClickListener {
            viewModel.readCatDetail()
        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        compositeDisposable.add(viewModel.dataSource.subscribeOn(Schedulers.io()).observeOn(
            AndroidSchedulers.mainThread()
        ).subscribe {
            when (it.state) {
                State.ERROR -> {
                    it.error?.let { message ->
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
                            .show()
                    }
                }
                else -> {
                    imageView.loadImage(it.data.image, R.drawable.ic_placeholder)
                    titleView.text = it.data.name
                    descriptionView.text = it.data.description
                    childFriendly.rating = it.data.childFriendly
                    energyLevel.rating = it.data.energyLevel
                }
            }
        })
    }
}
