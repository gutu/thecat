package com.cjuca.thecat.detail.ui.data

data class CatDetailUiData(
    val id: String = "",
    val name: String = "",
    val description: String = "",
    val energyLevel: Float = 0f,
    val image: String = "",
    val origin: String = "",
    val moreDetails: String = "",
    val childFriendly: Float = 0f
)