package com.cjuca.thecat.home.ui.recyclerviewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.cjuca.thecat.R
import com.cjuca.thecat.base.loadImage
import com.cjuca.thecat.home.ui.data.ImageItem
import kotlinx.android.synthetic.main.viewholder_image.view.*

class ImageRecyclerViewHolder(
    view: View
) :
    RecyclerView.ViewHolder(view) {


    fun bind(data: ImageItem) {
        itemView.imageView.loadImage(data.url, R.drawable.ic_placeholder)
    }
}