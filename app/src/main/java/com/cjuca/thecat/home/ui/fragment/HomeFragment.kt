package com.cjuca.thecat.home.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.cjuca.thecat.R
import com.cjuca.thecat.base.State
import com.cjuca.thecat.core.manager.CatsManager
import com.cjuca.thecat.home.ui.adapter.HomeAdapter
import com.cjuca.thecat.home.ui.viewmodel.HomeViewModel
import com.cjuca.thecat.home.ui.viewmodel.HomeViewModelFactory
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var homeAdapter: HomeAdapter
    private val compositeDisposable = CompositeDisposable()
    private val viewModelFactory by lazy {
        val context = this.requireActivity().application
        HomeViewModelFactory(context, CatsManager(context))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        homeAdapter = HomeAdapter(showCats = {
            viewModel.showCatsFromCategory(it)
        }, showAllCategories = {
            viewModel.refresh(pageNumber = 1)
        }, actionGif = {
            viewModel.showGifValue = it
            viewModel.refresh(pageNumber = 1)
        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = homeAdapter

        compositeDisposable.add(viewModel.dataSource.subscribeOn(Schedulers.io()).observeOn(
            AndroidSchedulers.mainThread()
        ).subscribe {
            when (it.state) {
                State.LOADING, State.REFRESHING -> {
                    loadingView.isVisible = true
                }
                State.ERROR -> {
                    loadingView.isVisible = false
                    it.error?.let { message ->
                        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
                            .show()
                    }
                }
                State.IDLE, State.SUCCESS, State.EMPTY -> {
                    loadingView.isVisible = false
                    homeAdapter.submitList(it.data.list)
                }
            }
        })

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
               // viewModel.loadMoreContent()
            }
        })
    }
}
