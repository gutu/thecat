package com.cjuca.thecat.home.ui.recyclerviewholder

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.cjuca.thecat.R
import com.cjuca.thecat.home.ui.data.CategoryItem
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup


class CategoryRecyclerViewHolder(
    view: View,
    private val showCats: (String) -> Unit = {},private val showAllCategories: () -> Unit = {}
) :
    RecyclerView.ViewHolder(view) {


    fun bind(data: CategoryItem) {
        val chipGroup = itemView.findViewById<ChipGroup>(R.id.tag_group)
        chipGroup.removeAllViews()

        data.list.map { chipItem ->
            val inflater =
                itemView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val chip = inflater.inflate(R.layout.row_chip_view, chipGroup, false) as Chip
            chip.text = chipItem.name
            chip.setOnCheckedChangeListener { compoundButton, isChecked ->
                if (isChecked) {
                    showCats.invoke(chipItem.id)
                } else {
                    showAllCategories.invoke()
                }
            }
            chipGroup.addView(chip)
        }
    }
}