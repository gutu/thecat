package com.cjuca.thecat.home.ui.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.cjuca.thecat.base.ScreenUiData
import com.cjuca.thecat.base.State
import com.cjuca.thecat.core.manager.CatsManager
import com.cjuca.thecat.core.model.CatCategory
import com.cjuca.thecat.core.model.Sort
import com.cjuca.thecat.core.model.Type
import com.cjuca.thecat.home.ui.data.DataHolder
import com.cjuca.thecat.home.ui.data.HomeUiData
import com.cjuca.thecat.home.ui.data.LoadingMore
import com.cjuca.thecat.home.ui.data.mapper.HomeMapper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers


class HomeViewModel(private val hostApplication: Application, private val manager: CatsManager) :
    AndroidViewModel(hostApplication) {

    val dataSource: BehaviorProcessor<ScreenUiData<HomeUiData>> = BehaviorProcessor.createDefault(
        ScreenUiData(
            state = State.LOADING, data = HomeUiData(
                emptyList()
            )
        )
    )
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
    private val mapper = HomeMapper()
    var showGifValue: Boolean = false
    private lateinit var dataHolder: DataHolder

    init {
        launchCategories()
    }

    fun loadMoreContent() {
        if (::dataHolder.isInitialized) {
            getNextPage()
        }
    }

    private fun getNextPage() {
        if (dataHolder.hasMoreContent()) {
            refresh(dataHolder.getNextPage())
        } else {
            dataSource.value?.let {
                dataSource.onNext(
                    it.copy(
                        state = State.EMPTY,
                        data = it.data.copy(list = emptyList())
                    )
                )
            }
        }
    }

    private fun launchCategories() {
        dataSource.value?.let { uiData ->
            dataSource.onNext(uiData.copy(state = State.REFRESHING))
            compositeDisposable.add(
                manager.getCategories().subscribeOn(Schedulers.io())
                    .subscribe({
                        launchAllImages(it)
                    }, {
                        Log.e("HomeViewModel", "Failed to load categories", it)
                        dataSource.onNext(
                            uiData.copy(
                                state = State.ERROR,
                                error = "Loading categories error : $it"
                            )
                        )
                    })
            )
        }
    }

    private fun launchAllImages(categories: List<CatCategory>) {
        dataSource.value?.let { uiData ->
            dataSource.onNext(uiData.copy(state = State.REFRESHING))
            compositeDisposable.add(
                manager.getAllImages(
                    20,
                    1,
                    Sort.DESC,
                    if (showGifValue) Type.ANIMATED.typeValue else Type.STATIC.typeValue
                ).subscribeOn(
                    Schedulers.io()
                )
                    .map {
                        mapper.mapToUiData(categories, it)
                    }
                    .subscribe({ list ->
                        dataHolder = DataHolder(100)
                        dataSource.onNext(
                            uiData.copy(
                                state = State.IDLE, data = uiData.data.copy(
                                    list = list
                                )
                            )
                        )
                    }, {
                        Log.e("HomeViewModel", "Failed to load all images", it)
                        dataSource.onNext(
                            uiData.copy(
                                state = State.ERROR,
                                error = "Loading all images error : $it"
                            )
                        )
                    })
            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun showCatsFromCategory(categoryId: String) {
        dataSource.value?.let { uiData ->
            dataSource.onNext(uiData.copy(state = State.REFRESHING))
            compositeDisposable.add(
                manager.getCatImages(
                    categoryId,
                    20,
                    1,
                    Sort.DESC,
                    if (showGifValue) Type.ANIMATED.typeValue else Type.STATIC.typeValue
                ).subscribeOn(Schedulers.io())
                    .map {
                        mapper.updateImages(uiData.data.list, it)
                    }
                    .subscribe({ list ->
                        dataSource.onNext(
                            uiData.copy(
                                state = State.IDLE, data = uiData.data.copy(
                                    list = list
                                )
                            )
                        )
                    }, {
                        Log.e("HomeViewModel", "Failed to load images", it)
                        dataSource.onNext(
                            uiData.copy(
                                state = State.ERROR,
                                error = "Loading images error : $it"
                            )
                        )
                    })
            )
        }
    }

    fun refresh(pageNumber: Int) {
        dataSource.value?.let { uiData ->
            dataSource.onNext(uiData.copy(state = State.REFRESHING))
            compositeDisposable.add(
                manager.getAllImages(
                    20,
                    pageNumber,
                    Sort.DESC,
                    if (showGifValue) Type.ANIMATED.typeValue else Type.STATIC.typeValue
                ).doFinally {
                    dataHolder.isLoadingInProgress = false
                }.subscribeOn(
                    Schedulers.io()
                )
                    .map {
                        mapper.updateImages(uiData.data.list, it)
                    }
                    .subscribe({ list ->
                        dataHolder.currentPage = pageNumber
                        if (dataHolder.hasMoreContent()) {
                           // list.add(LoadingMore)
                        }
                        dataSource.onNext(
                            uiData.copy(
                                state = State.IDLE, data = uiData.data.copy(
                                    list = list
                                )
                            )
                        )
                    }, {
                        Log.e("HomeViewModel", "Failed to load all images", it)
                        dataSource.onNext(
                            uiData.copy(
                                state = State.ERROR,
                                error = "Loading all images error : $it"
                            )
                        )
                    })
            )
        }
    }
}