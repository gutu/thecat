package com.cjuca.thecat.home.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cjuca.thecat.core.manager.CatsManager

class HomeViewModelFactory(
    private val application: Application,
    private val manager: CatsManager
) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(application, manager) as T
        }
        throw IllegalArgumentException("This factory handle only HomeViewModel classes")
    }
}