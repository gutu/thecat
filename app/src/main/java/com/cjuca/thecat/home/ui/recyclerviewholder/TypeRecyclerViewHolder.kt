package com.cjuca.thecat.home.ui.recyclerviewholder

import android.view.View
import androidx.core.view.forEach
import androidx.recyclerview.widget.RecyclerView
import com.cjuca.thecat.home.ui.data.TypeItem
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.viewholder_type.view.*

class TypeRecyclerViewHolder(
    view: View,
    private val actionGif: (showGif: Boolean) -> Unit = {}
) :
    RecyclerView.ViewHolder(view) {


    fun bind(data: TypeItem) {
        itemView.staticButton.setOnClickListener {
            actionGif.invoke(false)
        }
        itemView.gifButton.setOnClickListener {
            actionGif.invoke(true)
        }
    }
}