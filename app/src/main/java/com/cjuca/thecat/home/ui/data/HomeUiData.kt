package com.cjuca.thecat.home.ui.data

import com.cjuca.thecat.base.RecyclerItem
import java.util.*

enum class HomeItemType(val type: Int) {
    TYPE_SECTION(0), CATEGORY_SECTION(1), IMAGE_SECTION(2), LOADING_VIEW(3);

    companion object {
        fun valueOf(
            value: Int
        ): HomeItemType = HomeItemType.values().find { it.type == value }
            ?: throw IllegalArgumentException(
                "The searched value is not of type HomeItemType"
            )
    }
}

sealed class HomeRecyclerItem : RecyclerItem

data class HomeUiData(val list: List<HomeRecyclerItem> = emptyList())
data class ChipItem(val id: String, val name: String)

data class CategoryItem(
    val id: String,
    val list: List<ChipItem>
) :
    HomeRecyclerItem() {

    override fun getUniqueId(): Long = id.toLong()
    override fun isSameItem(
        other: RecyclerItem
    ): Boolean = other is CategoryItem && other.id == id

    override fun isSameContent(other: RecyclerItem): Boolean =
        other is CategoryItem && other == this

    override fun getType(): Int = HomeItemType.CATEGORY_SECTION.type
}

data class ImageItem(
    val id: String,
    val url: String
) :
    HomeRecyclerItem() {

    override fun getUniqueId(): Long = id.toLong()
    override fun isSameItem(
        other: RecyclerItem
    ): Boolean = other is ImageItem && other.id == id

    override fun isSameContent(other: RecyclerItem): Boolean =
        other is ImageItem && other == this

    override fun getType(): Int = HomeItemType.IMAGE_SECTION.type
}

data class TypeItem(
    val id: String
) :
    HomeRecyclerItem() {

    override fun getUniqueId(): Long = id.toLong()
    override fun isSameItem(
        other: RecyclerItem
    ): Boolean = other is TypeItem && other.id == id

    override fun isSameContent(other: RecyclerItem): Boolean =
        other is TypeItem && other == this

    override fun getType(): Int = HomeItemType.TYPE_SECTION.type
}

object LoadingMore : HomeRecyclerItem() {
    private val id: Long = UUID.randomUUID()
        .mostSignificantBits

    override fun isSameContent(other: RecyclerItem): Boolean = true

    override fun getType(): Int = HomeItemType.LOADING_VIEW.type

    override fun getUniqueId(): Long = id

    override fun isSameItem(other: RecyclerItem): Boolean = true
}

class DataHolder(
    val numberOfPages: Int = 0, var currentPage: Int = 0,
    var isLoadingInProgress: Boolean = false
) {
    fun hasMoreContent() = currentPage < numberOfPages

    fun getNextPage() = currentPage.inc()
}