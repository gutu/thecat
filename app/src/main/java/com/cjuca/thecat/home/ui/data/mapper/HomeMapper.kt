package com.cjuca.thecat.home.ui.data.mapper

import com.cjuca.thecat.core.model.CatCategory
import com.cjuca.thecat.core.model.CatImage
import com.cjuca.thecat.home.ui.data.*
import java.util.*

class HomeMapper {
    fun mapToUiData(categories: List<CatCategory>, images: List<CatImage>): List<HomeRecyclerItem> {
        val finalList = mutableListOf<HomeRecyclerItem>()
        finalList.add(TypeItem(id = UUID.randomUUID().mostSignificantBits.toString()))

        val chipList = mutableListOf<ChipItem>()
        categories.map {
            chipList.add(ChipItem(it.id.toString(), it.name))
        }
        finalList.add(
            CategoryItem(
                id = UUID.randomUUID().mostSignificantBits.toString(),
                list = chipList
            )
        )

        images.map {
            finalList.add(ImageItem(id = it.id, url = it.url))
        }
        return finalList
    }

    fun updateImages(
        actualList: List<HomeRecyclerItem>,
        newImageList: List<CatImage>
    ): List<HomeRecyclerItem> {
        val finalList = mutableListOf<HomeRecyclerItem>()
        actualList.filterIsInstance(TypeItem::class.java).map {
            finalList.add(it)
        }
        actualList.filterIsInstance(CategoryItem::class.java).map {
            finalList.add(it)
        }
        newImageList.map {
            finalList.add(ImageItem(id = it.id, url = it.url))
        }
        return finalList
    }
}