package com.cjuca.thecat.home.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cjuca.thecat.R
import com.cjuca.thecat.home.ui.data.*
import com.cjuca.thecat.home.ui.recyclerviewholder.CategoryRecyclerViewHolder
import com.cjuca.thecat.home.ui.recyclerviewholder.ImageRecyclerViewHolder
import com.cjuca.thecat.home.ui.recyclerviewholder.LoadingRecyclerViewHolder
import com.cjuca.thecat.home.ui.recyclerviewholder.TypeRecyclerViewHolder

class HomeAdapter(
    private val showCats: (String) -> Unit = {},
    private val showAllCategories: () -> Unit = {},
    private val actionGif: (showGif: Boolean) -> Unit = {}
) :
    ListAdapter<HomeRecyclerItem, RecyclerView.ViewHolder>(diffCallback) {
    companion object {

        private val diffCallback = object : DiffUtil.ItemCallback<HomeRecyclerItem>() {
            override fun areItemsTheSame(
                oldItem: HomeRecyclerItem,
                newItem: HomeRecyclerItem
            ): Boolean {
                if (oldItem.getType() == newItem.getType()) {
                    return oldItem.isSameItem(newItem)
                }
                return false
            }

            override fun areContentsTheSame(
                oldItem: HomeRecyclerItem,
                newItem: HomeRecyclerItem
            ): Boolean {
                return oldItem.isSameContent(newItem)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val type = HomeItemType.valueOf(viewType)
        return when (type) {
            HomeItemType.TYPE_SECTION -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.viewholder_type, parent, false)
                TypeRecyclerViewHolder(
                    view,
                    actionGif
                )
            }
            HomeItemType.CATEGORY_SECTION -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.viewholder_category, parent, false)
                CategoryRecyclerViewHolder(
                    view,
                    showCats, showAllCategories
                )
            }
            HomeItemType.IMAGE_SECTION -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.viewholder_image, parent, false)
                ImageRecyclerViewHolder(
                    view
                )
            }
            HomeItemType.LOADING_VIEW -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.viewholder_loading, parent, false)
                LoadingRecyclerViewHolder(
                    view
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = getItem(position)
        when (data) {
            is TypeItem -> {
                val viewHolder = holder as TypeRecyclerViewHolder
                viewHolder.bind(data)
            }
            is CategoryItem -> {
                val viewHolder = holder as CategoryRecyclerViewHolder
                viewHolder.bind(data)
            }
            is ImageItem -> {
                val viewHolder = holder as ImageRecyclerViewHolder
                viewHolder.bind(data)
            }
            is LoadingMore   -> {
                //nothing to do
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).getType()
    }

    public override fun getItem(position: Int): HomeRecyclerItem {
        return super.getItem(position)
    }
}
