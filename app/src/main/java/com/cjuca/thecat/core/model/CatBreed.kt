package com.cjuca.thecat.core.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CatBreed(
    var id: String,
    var name: String,
    var origin: String,
    var description: String,
    @SerializedName("wikipedia_url") var moreDetails: String,
    @SerializedName("energy_level") var energy: Float,
    @SerializedName("child_friendly") var childFriendly: Float

) :
    Serializable {}