package com.cjuca.thecat.core.manager

import android.content.Context
import com.cjuca.thecat.core.api.ApiService
import com.cjuca.thecat.core.model.CatCategory
import com.cjuca.thecat.core.model.CatImage
import com.cjuca.thecat.core.model.Sort
import io.reactivex.Single
import kotlin.properties.Delegates

class CatsManager(context: Context) {

    private var apiService by Delegates.notNull<ApiService>()

    init {
        apiService = ApiService.create(context)
    }

    fun getCatImages(
        categoryId: String,
        limit: Long,
        page: Int,
        sort: Sort, type: String
    ): Single<List<CatImage>> =
        apiService.getCatImages(categoryId, limit, page, sort.sortValue, type)

    fun getAllImages(
        limit: Long,
        page: Int,
        sort: Sort, type: String
    ): Single<List<CatImage>> =
        apiService.getCatImages(null, limit, page, sort.sortValue, type)

    fun getCategories(): Single<List<CatCategory>> = apiService.getCategories()

    fun getCatDetail(catId: String): Single<CatImage> = apiService.getCatDetail(catId)

}