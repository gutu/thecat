package com.cjuca.thecat.core.model

import java.io.Serializable

data class CatCategory(
    var id: Long, var name: String
) :
    Serializable {}