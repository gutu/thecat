package com.cjuca.thecat.core.model

import java.io.Serializable

data class CatImage(
    var id: String,
    var url: String,
    var breeds: List<CatBreed> = emptyList()
) :
    Serializable {}