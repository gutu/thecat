package com.cjuca.thecat.core.model

enum class Sort(var sortValue: String) {
    NO_SORT(""), DESC("desc"), ASC("asc");
}

enum class Type(var typeValue: String) {
    STATIC("jpg,png"), ANIMATED("gif");
}