package com.cjuca.thecat.core.api

import android.content.Context
import com.cjuca.thecat.R
import com.cjuca.thecat.core.model.CatCategory
import com.cjuca.thecat.core.model.CatImage
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET("v1/images/search")
    fun getCatImages(
        @Query("category_ids") categoryId: String?, @Query("limit") limit: Long, @Query("page") page: Int, @Query(
            "order"
        ) order: String, @Query("mime_types") type: String
    ): Single<List<CatImage>>

    @GET("v1/categories")
    fun getCategories(): Single<List<CatCategory>>

    @GET("v1/images/{image_id}")
    fun getCatDetail(@Path("image_id") catId: String): Single<CatImage>

    companion object {
        fun create(context: Context): ApiService {
            val loggerInterceptor = HttpLoggingInterceptor()
            loggerInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
            loggerInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder().apply {
                addInterceptor(
                    Interceptor { chain ->
                        val builder = chain.request().newBuilder()
                        builder.header("X-Api-Key", "eb2d45a9-2bb1-4321-8d33-527e54ac0631")
                        return@Interceptor chain.proceed(builder.build())
                    }
                )
                addNetworkInterceptor(loggerInterceptor)
            }.build()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(context.getString(R.string.configurationUrl))
                .client(okHttpClient)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}